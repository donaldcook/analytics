.snowflake_dbt_jobs: &snowflake_dbt_jobs
  image: registry.gitlab.com/gitlab-data/data-image/dbt-image:latest
  stage: dbt_run
  before_script:
    - cd transform/snowflake-dbt/
    - echo $SNOWFLAKE_DATABASE
    - if [ $SNOWFLAKE_DATABASE = "master" ]; then export SNOWFLAKE_TRANSFORM_DATABASE="ANALYTICS"; else export SNOWFLAKE_TRANSFORM_DATABASE="${CI_COMMIT_REF_NAME^^}_ANALYTICS"; fi
    - echo $SNOWFLAKE_TRANSFORM_DATABASE
    - if [ $SNOWFLAKE_DATABASE = "master" ]; then export snowflake_load_database="RAW"; else export snowflake_load_database="${CI_COMMIT_REF_NAME^^}_RAW"; fi
    - echo $snowflake_load_database # This is a workaround for a bug in dbt https://github.com/fishtown-analytics/dbt/issues/1712
    - export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XS_WAREHOUSE
    - echo $SNOWFLAKE_TRANSFORM_WAREHOUSE
    - export CI_PROFILE_TARGET="--profiles-dir profile --target ci"
    - echo $CI_PROFILE_TARGET
  tags:
    - analytics
  only:
    - merge_requests
  when: manual

# Literal block abstraction
.deps_and_seed: &deps_and_seed |
  python3 macro_name_check.py
  echo -e "\e[92mdbt deps $CI_PROFILE_TARGET\033[0m"
  dbt deps $CI_PROFILE_TARGET
  echo -e "\e[92mdbt seed $CI_PROFILE_TARGET\033[0m"
  dbt seed $CI_PROFILE_TARGET #seed data from csv

.xl_warehouse: &xl_warehouse |
  export SNOWFLAKE_TRANSFORM_WAREHOUSE=$SNOWFLAKE_MR_XL_WAREHOUSE
  echo -e "\e[32mecho \$SNOWFLAKE_TRANSFORM_WAREHOUSE\033[0m"
  echo $SNOWFLAKE_TRANSFORM_WAREHOUSE


# MR Jobs
all:
  <<: *snowflake_dbt_jobs
  script:
    - *xl_warehouse
    - *deps_and_seed
    - dbt run $CI_PROFILE_TARGET --full-refresh || true
    - dbt test $CI_PROFILE_TARGET || true
    - dbt run $CI_PROFILE_TARGET || true
    - dbt test $CI_PROFILE_TARGET

exclude_product:  
  <<: *snowflake_dbt_jobs
  script:
    - *deps_and_seed
    - dbt run $CI_PROFILE_TARGET --full-refresh --exclude tag:product || true
    - dbt test $CI_PROFILE_TARGET --exclude tag:product || true
    - dbt run $CI_PROFILE_TARGET --exclude tag:product || true
    - dbt test $CI_PROFILE_TARGET --exclude tag:product

exclude_snowplow:
  <<: *snowflake_dbt_jobs
  script:
    - *xl_warehouse
    - *deps_and_seed
    - dbt run $CI_PROFILE_TARGET --full-refresh --exclude snowplow || true
    - dbt test $CI_PROFILE_TARGET --exclude snowplow || true
    - dbt run $CI_PROFILE_TARGET --exclude snowplow || true
    - dbt test $CI_PROFILE_TARGET --exclude snowplow

gitlab_dotcom:
  <<: *snowflake_dbt_jobs
  script:
    - *xl_warehouse
    - *deps_and_seed
    - dbt run $CI_PROFILE_TARGET --models date_details
    - dbt run $CI_PROFILE_TARGET --full-refresh --models gitlab_dotcom || true
    - dbt test $CI_PROFILE_TARGET --models gitlab_dotcom || true
    - dbt run $CI_PROFILE_TARGET --models gitlab_dotcom || true
    - dbt test $CI_PROFILE_TARGET --models gitlab_dotcom


pings:
  <<: *snowflake_dbt_jobs
  script:
    - *xl_warehouse
    - *deps_and_seed
    - dbt run $CI_PROFILE_TARGET --full-refresh --models pings || true
    - dbt test $CI_PROFILE_TARGET --models pings || true
    - dbt run $CI_PROFILE_TARGET --models pings || true
    - dbt test $CI_PROFILE_TARGET --models pings

snowplow:
  <<: *snowflake_dbt_jobs
  script:
    - *xl_warehouse
    - *deps_and_seed
    - dbt run $CI_PROFILE_TARGET --full-refresh --models snowplow || true
    - dbt test $CI_PROFILE_TARGET --models snowplow || true
    - dbt run $CI_PROFILE_TARGET --models snowplow || true
    - dbt test $CI_PROFILE_TARGET --models snowplow

specify_exclude:
  <<: *snowflake_dbt_jobs
  script:
    - *deps_and_seed
    - echo $DBT_MODELS
    - dbt run $CI_PROFILE_TARGET --full-refresh --exclude $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --exclude $DBT_MODELS || true
    - dbt run $CI_PROFILE_TARGET --exclude $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --exclude $DBT_MODELS

specify_xl_exclude:
  <<: *snowflake_dbt_jobs
  script:
    - *xl_warehouse
    - *deps_and_seed
    - echo $DBT_MODELS
    - dbt run $CI_PROFILE_TARGET --full-refresh --exclude $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --exclude $DBT_MODELS || true
    - dbt run $CI_PROFILE_TARGET --exclude $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --exclude $DBT_MODELS

specify_model:
  <<: *snowflake_dbt_jobs
  script:
    - *deps_and_seed
    - echo $DBT_MODELS
    - dbt run $CI_PROFILE_TARGET --full-refresh --models $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --models $DBT_MODELS || true
    - dbt run $CI_PROFILE_TARGET --models $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --models $DBT_MODELS

specify_xl_model:
  <<: *snowflake_dbt_jobs
  script:
    - *xl_warehouse
    - *deps_and_seed
    - echo $DBT_MODELS
    - dbt run $CI_PROFILE_TARGET --full-refresh --models $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --models $DBT_MODELS || true
    - dbt run $CI_PROFILE_TARGET --models $DBT_MODELS || true
    - dbt test $CI_PROFILE_TARGET --models $DBT_MODELS

# dbt tests
all_tests:
  <<: *snowflake_dbt_jobs
  stage: dbt_misc
  script:
    - *deps_and_seed
    - dbt test $CI_PROFILE_TARGET

data_tests:
  <<: *snowflake_dbt_jobs
  stage: dbt_misc
  script:
    - *deps_and_seed
    - dbt test --data $CI_PROFILE_TARGET

freshness:
  <<: *snowflake_dbt_jobs
  stage: dbt_misc
  script:
    - *deps_and_seed
    - dbt source snapshot-freshness $CI_PROFILE_TARGET

schema_tests:
  <<: *snowflake_dbt_jobs
  stage: dbt_misc
  script:
    - *deps_and_seed
    - dbt test --schema $CI_PROFILE_TARGET

# snapshots:
#   <<: *snowflake_dbt_jobs
#   stage: dbt_misc
#   script:
#     - *deps_and_seed
#     - echo $snowflake_load_database
#     - dbt snapshot $CI_PROFILE_TARGET
    
specify_tests:
  <<: *snowflake_dbt_jobs
  stage: dbt_misc
  script:
    - *deps_and_seed
    - echo $DBT_MODELS
    - dbt test $CI_PROFILE_TARGET --models $DBT_MODELS
