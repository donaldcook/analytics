{% docs zendesk_org_desc %}

The organizations table contains information about the organizations your end-users belong to. [Link to Documentation](https://www.stitchdata.com/docs/integrations/saas/zendesk#organizations)

{% enddocs %}


{% docs zendesk_tickets_desc %}

The tickets table contains info about the tickets in your Zendesk Support account. Tickets are the means through which your end users (customers) communicate with your Zendesk Support agents. [Link to Documentation](https://www.stitchdata.com/docs/integrations/saas/zendesk#tickets)

{% enddocs %}

{% docs zendesk_users_desc %}

The users table contains info about the users associated with your Zendesk Support account. This includes agents, admins, and end-users (customers). [Link to Documentation](https://www.stitchdata.com/docs/integrations/saas/zendesk#users)

{% enddocs %}

{% docs zendesk_ticket_metrics_desc %}

The ticket_metrics table contains info about the metrics associated with Zendesk Support tickets. This table will not include records for deleted tickets. [Link to Documentation](https://www.stitchdata.com/docs/integrations/saas/zendesk#ticket-metrics)

{% enddocs %}
