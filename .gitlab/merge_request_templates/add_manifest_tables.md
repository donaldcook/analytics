Closes

* List the tables added/changed below
* Run the `clone_raw` CI job
* Run the `pgp_test` CI job. Include the `MANIFEST_NAME` variable and input the name of the db (i.e. `gitlab_com`)

#### Tables Changed/Added

* [ ] List

#### PGP Test CI job passed?

* [ ] List
