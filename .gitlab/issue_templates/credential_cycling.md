<!---
This issue is for cycling credentials within our data systems.
---->

#### Places that credentials need to be cycled:

* [ ] 1Password
* [ ] Analytics project CI Variables
* [ ] Data Infrastructure project CI Variables
* [ ] Data Utils project CI Variables
* [ ] Chatops project CI Variables
* [ ] Kube secrets (default namespace)
* [ ] Kube secrets (testing namespace)
* [ ] Stitch